package model.logic;

import sun.security.x509.GeneralName;

public class Trips {

	private int tripId;
	
	private String startTime;
	
	private String finalTime;
	
	private int bikeId;
	
	private int tripDuration;
	
	private int fromStationId;
	
	private String fromStationName;
	
	private int toStationId;
	
	private String toStationName;
	
	private String userType;
	
	private String gender;
	
	private int birthYear;
	
	public Trips(int pTripId,String pStartTime,String pFinalTime,int pBikeId,int pTripDuration,int pFromStationId,String pFromStationName,int pToStationId,String pToStationName,String pUserType,String pGender,int pBirthYear)
	{
		tripId = pTripId;
		startTime = pStartTime;
		finalTime = pFinalTime;
		bikeId = pBikeId;
		tripDuration = pTripDuration;
		fromStationId = pFromStationId;
		fromStationName = pFromStationName;
		toStationId = pToStationId;
		toStationName = pToStationName;
		userType = pUserType;
		gender = pGender;
		birthYear = pBirthYear;
				
	}
	
	public int darTripId()
	{
		return tripId;
	}
	
	public String darStartTime()
	{
		return startTime;
	}
	
	public String darFinalTime()
	{
		return finalTime;
	}
	
	public int darBikeId()
	{
		return bikeId;
	}
	
	public int darTripDuration()
	{
		return tripDuration;
	}
	
	public int darFromStationId()
	{
		return fromStationId;
	}
	
	public String darFromStationName()
	{
		return fromStationName;
	}
	
	public int darToStationId()
	{
		return toStationId;
	}
	
	public String darToStationName()
	{
		return toStationName;
	}
	
	public String darUserType()
	{
		return userType;
	}
	
	public String darGender()
	{
		return gender;
	}
	
	public int darBirthYear()
	{
		return birthYear;
	}
}
