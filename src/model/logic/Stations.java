package model.logic;


public class Stations{
	public int id;
	
	public String name;
	
	public String city;
	
	public double latidude;
	
	public double longitude;
	
	public int capacity;
	
	public String date;
	
	public Stations(int num,String pName,String ciudad,double lat, double longi, int cap,String fecha)
	{
		id = num;
		name = pName;
		city = ciudad;
		latidude = lat;
		longitude = longi;
		capacity = cap;
		date = fecha;
	}
	
	public int darId()
	{
		return id;
	}
	
	public String darNombre()
	{
		return name;
	}
	public String darCiudad()
	{
		return city;
	}
	
	public double darLatitud()
	{
		return latidude;
	}
	
	public double darLongitud()
	{
		return longitude;
	}
	
	public int darCapacidad()
	{
		return capacity;
	}
	
	public String darFecha()
	{
		return date;
	}
}
