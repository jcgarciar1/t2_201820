package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

import api.IDivvyTripsManager;
import model.vo.VOTrip;
import model.data_structures.DoublyLinkedList;
import model.data_structures.LinkedList;
import model.data_structures.Node;

public class DivvyTripsManager implements IDivvyTripsManager {


	private LinkedList stations;

	private LinkedList trips;

	public DivvyTripsManager()
	{
		stations = new LinkedList<>();

		trips = new LinkedList<>();
	}

	public void loadStations (String stationsFile) {
		// TODO Auto-generated method stub
		int contador=0;

		try {
			FileReader h = new FileReader(stationsFile);
			BufferedReader bf = new BufferedReader(h);
			String entrada = bf.readLine();
			entrada = bf.readLine();
			while (entrada != null) {
				String[] texto = entrada.toString().split(",");
				int id = Integer.parseInt(texto[0]);
				double lat = Double.parseDouble(texto[3]);
				double longi = Double.parseDouble(texto[4]);
				int cap = Integer.parseInt(texto[5]);
				Stations a = new Stations(id, texto[1], texto[2], lat, longi, cap, texto[6]);
				entrada = bf.readLine();

				if(contador == 0)
				{
					stations.addFirst(a);
					contador++;
				}
				else
					stations.append(a);
			}		
				bf.close();
				h.close();
		}
		catch (IOException fnE) {
			System.out.println(fnE.getMessage());

		}

	}




	public void loadTrips (String tripsFile) {
		// TODO Auto-generated method stub

		int contador=0;

		try {
			FileReader h = new FileReader(tripsFile);
			BufferedReader bf = new BufferedReader(h);
			String entrada = bf.readLine();
			entrada = bf.readLine();
			while (entrada != null) {
				String[] texto = entrada.toString().split(",");
				int pTripId = Integer.parseInt(texto[0]);
				String pStartTime = texto[1];
				String pFinalTime = texto[2];
				int pBikeId = Integer.parseInt(texto[3]);
				int pTripDuration = Integer.parseInt(texto[4]);
				int pFromStationId = Integer.parseInt(texto[5]);
				String pFromStationName = texto[6];
				int pToStationId = Integer.parseInt(texto[7]);
				String pToStationName = texto[8];
				String pUserType = texto[9];
				String pGender = "";
				int pBirthYear = 0;
				if(pUserType.equals("Subscriber"))
				{
						pGender = texto[10];
						pBirthYear = Integer.parseInt(texto[11]);
				}
				Trips a = new Trips(pTripId, pStartTime, pFinalTime, pBikeId, pTripDuration, pFromStationId, pFromStationName, pToStationId, pToStationName, pUserType, pGender, pBirthYear);
				if(contador == 0)
				{
					stations.addFirst(a);
					contador++;
				}
				else
					stations.append(a);

				entrada = bf.readLine();
			}
				bf.close();
				h.close();
		}
		catch (IOException e) {
			System.out.println(e.getMessage());
		}

	}

	@Override
	public DoublyLinkedList <VOTrip> getTripsOfGender (String gender) {
		// TODO Auto-generated method stub
		LinkedList a = new LinkedList<>();
		if(gender.equals("male"))
		{
			for (int i = 0; i < trips.size(); i++) {
				Trips b = (Trips) trips.get(i).getItem();
				
				if(b.darGender().equals(gender))
				{
					a.addFirst(b);
				}
			}
		}
		else
		{
			for (int i = 0; i < trips.size(); i++) {
				Trips b = (Trips) trips.get(i).getItem();
				if(b.darGender().equals(gender))
				{
					a.addFirst(b);
				}
			}

		}
		return a;
		}

	@Override
	public DoublyLinkedList <VOTrip> getTripsToStation (int stationID) {
		// TODO Auto-generated method stub
		LinkedList a = new LinkedList<>();
		for (int i = 0; i < stations.size(); i++) {
			Trips b = (Trips) trips.get(i).getItem();
			if(b.darToStationId() == stationID)
			{
				a.addFirst(b);
			}
		}
		return a;
		}	

	public LinkedList darStations()
	{
		return stations;
	}

	public LinkedList darTrips()
	{
		return trips;
	}

}
