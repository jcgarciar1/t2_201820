package model.data_structures;

/**
 * Abstract Data Type for a doubly-linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface DoublyLinkedList<T> extends Iterable<T> {

	//Tomado de presentaciones en clase
	Integer getSize();
	
	public void addFirst(T item);
	public void append(T item);
	public void addAtPosition(T item, int i);
	public void removeFirst();
	public void remove (int pos);
	public Node<T> get (int pos);
	public int size();
	public Node next();
	public Node previous();
	public boolean isEmpty();
	public Node darPrimero();
	public Node darUltimo();
	

}		


