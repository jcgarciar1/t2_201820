package model.data_structures;

public class Node<E> {
	private Node<E> next;
	private Node<E> anterior;
	private E item;
	
	public Node(E item)
	{
		next = null;
		anterior = null;
		this.item = item;
	}
	public Node<E> getNext() {
	return next;
	}
	
	public void setNextNode ( Node<E> next) {
	this.next = next;
	} 
	
	public Node<E> getPrevious()
	{
		return anterior;
	}
	public void setPreviousNode (Node<E> prev)
	{
		this.anterior = prev;
	}
	
	public E getItem(){
	return item;
	}
	
	public void setItem (E item) 
	{
	this.item = item;
	}
}
