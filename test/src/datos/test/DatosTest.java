package datos.test;


import junit.framework.TestCase;
import model.data_structures.DoublyLinkedList;
import model.data_structures.LinkedList;
import model.data_structures.Node;
import model.logic.DivvyTripsManager;
import model.logic.Stations;

public class DatosTest extends TestCase {

	private DivvyTripsManager datos;

	private DoublyLinkedList<Integer> list;

	/**
	 * Inicializa datos
	 */

	public void setupScenario1()
	{
		list = new LinkedList<>();
	}

	public void testStations()
	{
		setupScenario1();
		list.addFirst(1);
		list.append(2);
		assertTrue(list.getSize()==2);
		assertNotNull(list);
		assertTrue(list.darPrimero().getNext() == list.darUltimo());
		assertNull(list.darPrimero().getPrevious());
		list.addFirst(5);
		assertTrue((Integer)list.darPrimero().getItem()==5);
		list.addAtPosition(8, 2);
		assertTrue((Integer)list.get(2).getItem() == 8);
		list.removeFirst();
		assertFalse((Integer)list.darPrimero().getItem()==5);
		list.addAtPosition(11, 2);
		list.remove(2);
		assertFalse((Integer)list.get(2).getItem() == 11);
		assertFalse(list.isEmpty());
		list = new LinkedList<>();
		assertTrue(list.isEmpty());
	}

}
